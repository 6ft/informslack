import os
import os.path
from setuptools import setup, find_packages

readme_file = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'README.md')
setup(
    description='informslack',
    version='0.0.2',
    long_description=open(readme_file).read(),
    author='Six Foot Platform',
    author_email='dev@6ft.com',
    name='informslack',
    url='https://bitbucket.org/6ft/informslack',
    packages=find_packages(),
    include_package_data=True,
    install_requires=['slackclient'],
    license="Proprietary",
    keywords="",
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'Programming Language :: Python',
    ],
)
