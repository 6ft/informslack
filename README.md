# INFORMSLACK

This is a quick and dirty way to provide cli access to your slack channels.

1. create a slack app with a bot
2. install this on your server, in the global python env is fine (pip install git+https://bitbucket.org/6ft/informslack.git)
3. run `sudo python3.6 -m informslack config config config` to create a basic ini file in `/etc/informslack/config.ini`.
4. edit `/etc/informslack/config.ini` to have your app's bot's oauth access token
5. edit `/etc/informslack/config.ini` to map your channel names to the channel ids (just look at the ID in the slack channel url)
6. `python3.6 -m informslack testchannel say "This is a test message."`
7. use the timestamp this returns to do things.
8. `python3.6 -m informslack testchannel add-reaction thumbsup -t 1520026229.000520`
9. update all your server scripts to do things like this.


## Use Case: Pending Actions
Say you're restarting a server.

1. kill server pid or whatever
2. ts=`python3.6 -m informslack testchannel say "Server restart."`
3. `python3.6 -m informslack testchannel add-reaction loading -t $ts`
4. server starts
5. `python3.6 -m informslack testchannel remove-reaction loading -t $ts`
6. `python3.6 -m informslack testchannel add-reaction heavy_check_mark -t $ts`

