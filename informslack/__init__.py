import sys
import os.path
import configparser
from slackclient import SlackClient


CONFIG_PATH = '/etc/informslack/config.ini'


def load_config():
    if not os.path.exists(CONFIG_PATH):
        sys.stderr.write('No config found at {}. A default one will be created. It will not be functional until proper values are added.\n'.format(CONFIG_PATH))
        dirp = os.path.dirname(CONFIG_PATH)
        os.makedirs(dirp)
        fh = open(CONFIG_PATH, 'w+')
        fh.write('[slack]\nkey = _apikey_\n\n[channelname]\nid = _C0XXXX_')
        fh.close()

    config = configparser.ConfigParser()
    config.read(CONFIG_PATH)
    return config

def get_channel_config(config, channel_name):
    sections = config.sections()
    if channel_name in sections:
        return config[channel_name]
    else:
        return None

def say(channel_name, text):
    config = load_config()
    slack_token = config['slack']['key']
    sc = SlackClient(slack_token)

    channel_config = get_channel_config(config, channel_name)
    if channel_config is None:
        sys.stderr.write('No channel config by the name "{}".\n'.format(channel_name))
        return None

    res = sc.api_call('chat.postMessage',
        channel=channel_config['id'],
        text=text,
        as_user=False,
    )
    if res['ok']:
        return res['ts']
    else:
        sys.stderr.write('Error "{}".\n'.format(res['error']))
        return None

def add_reaction(channel_name, timestamp, emoji):
    config = load_config()
    slack_token = config['slack']['key']
    sc = SlackClient(slack_token)

    channel_config = get_channel_config(config, channel_name)
    if channel_config is None:
        sys.stderr.write('No channel config by the name "{}".\n'.format(channel_name))
        return None

    sc.api_call(
        "reactions.add",
        channel=channel_config['id'],
        name=emoji,
        timestamp=timestamp,
        as_user=False,
    )

def remove_reaction(channel_name, timestamp, emoji):
    config = load_config()
    slack_token = config['slack']['key']
    sc = SlackClient(slack_token)

    channel_config = get_channel_config(config, channel_name)
    if channel_config is None:
        sys.stderr.write('No channel config by the name "{}".\n'.format(channel_name))
        return None

    sc.api_call(
        "reactions.remove",
        channel=channel_config['id'],
        name=emoji,
        timestamp=timestamp,
        as_user=False,
    )