import sys
import argparse
from . import *

parser = argparse.ArgumentParser()
parser.add_argument('channel', help='A channel name to send the message to. These must be in configured as keys in "{}"'.format(CONFIG_PATH))
parser.add_argument('action', help='What to do: options are "say", "add-reaction", "remove-reaction"')
parser.add_argument('value', help='For "say" this is the text to say. For "add-reaction" and "remove-reaction" this is what emoji to use.')
parser.add_argument('-t', '--ts', help='The timestamp of an existing message to edit.', default=0, nargs='?')

cli_args = parser.parse_args()

if cli_args.action == 'say':
    ts = say(cli_args.channel, cli_args.value)
    if ts is not None:
        sys.stdout.write(ts)

elif cli_args.action == 'add-reaction':
    ts = cli_args.ts
    add_reaction(cli_args.channel, ts, cli_args.value)

elif cli_args.action == 'remove-reaction':
    ts = cli_args.ts
    remove_reaction(cli_args.channel, ts, cli_args.value)

